
__name__ = 'display_flex'
name_url = __name__.replace('_', '-')

__version__ = '0.2.2'

__description__ = 'Improved IPython.display.display with CSS Flexbox layout'
__long_description__ = 'See README'
__author__ = 'oscar6echo'
__author_email__ = 'olivier.borderies@gmail.com'
__url__ = 'https://gitlab.com/oscar6echo/{}'.format(name_url)
__download_url__ = 'https://gitlab.com/oscar6echo/{}/repository/archive.tar.gz?ref={}'.format(name_url,
                                                                                              __version__)
__keywords__ = ['python', 'display', 'html', 'css', 'flexbox']
__license__ = 'MIT'
__classifiers__ = ['Development Status :: 4 - Beta',
                   'License :: OSI Approved :: MIT License',
                   'Programming Language :: Python :: 2.7',
                   'Programming Language :: Python :: 3.5',
                   'Programming Language :: Python :: 3.6'
                   ]
__include_package_data__ = True
__package_data__ = {}
__zip_safe__ = False
__entry_points__ = {}
